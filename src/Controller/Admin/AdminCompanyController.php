<?php

namespace App\Controller\Admin;

use App\Entity\Company;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/companies",name="admin_companies_")
 */
class AdminCompanyController extends AbstractController
{
    private $company;

    public function __construct(CompanyRepository $company)
    {
        $this->company = $company;
    }

    /**
     * @Route("/",name="list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $companies = $this->company->findAll();
        //dump($companies);
        return $this->render("admin/company/companies.html.twig", compact("companies"));
    }

    /**
     * Form processing for the creation of a new customer
     * @param FormInterface $form
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function processform(FormInterface $form, Company $company)
    {
        $em = $this->getDoctrine()->getManager();
        if (is_null($company->getId())) {
            $em->persist($company);
            $this->addFlash("success", "Entreprise créée avec succès");
        } else {
            $em->persist($form->getData());
            $this->addFlash("success", "Entreprise modifiée avec succès");
        }
        $em->flush();

        return $this->redirectToRoute("admin_companies_list");
    }

    /**
     * @Route("/create",name="create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $company = new Company();
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form, $company);
        }
        $forms = $form->createView();
        return $this->render("admin/company/companycreate.html.twig", compact("company", "forms"));
    }

    /**
     * @Route("/edit/{id}",name="edit")
     * 
     * @param Request $request
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Company $company, Request $request)
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processform($form, $company);
        }
        $forms = $form->createView();
        return $this->render("admin/company/companyedit.html.twig", compact("company", "forms"));
    }

    /**
     * @Route("/delete/{id}",name="delete")
     * @param Request $request
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Company $company, Request $request)
    {
        if ($this->isCsrfTokenValid("delete" . $company->getId(), $request->get("_token"))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($company);
            $em->flush();
        }
        return $this->redirectToRoute("admin_companies_list");
    }


    
}