<?php

namespace App\Security\Voter;


use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class VotesVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['NominationYear'])
            && $subject instanceof \App\Entity\NominationYear;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }
        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'NominationYear':
                $userVotes = $user->getVotes();
                foreach ($userVotes as $userVote)
                {
                    $nominationYearId =$userVote->getNominationYear()->getId();

                        if ($nominationYearId == $subject->getId())
                        {
                            return true;
                        }
                }
                break;
        }

        return false;
    }

}

