<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200529101945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, personal_code VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, document VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, zip_code VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, web_site VARCHAR(255) NOT NULL, picture VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, document VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, category VARCHAR(255) NOT NULL, document VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE votes (id INT AUTO_INCREMENT NOT NULL, nomination_year_id INT DEFAULT NULL, company_id INT DEFAULT NULL, user_id INT DEFAULT NULL, date DATETIME NOT NULL, INDEX IDX_518B7ACF55CDE221 (nomination_year_id), INDEX IDX_518B7ACF979B1AD6 (company_id), INDEX IDX_518B7ACFA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nomination_year (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, date_nomination_year DATETIME NOT NULL, INDEX IDX_6F1AD43912469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nomination_year_company (nomination_year_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_47D6465355CDE221 (nomination_year_id), INDEX IDX_47D64653979B1AD6 (company_id), PRIMARY KEY(nomination_year_id, company_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE votes ADD CONSTRAINT FK_518B7ACF55CDE221 FOREIGN KEY (nomination_year_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE votes ADD CONSTRAINT FK_518B7ACF979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE votes ADD CONSTRAINT FK_518B7ACFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE nomination_year ADD CONSTRAINT FK_6F1AD43912469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE nomination_year_company ADD CONSTRAINT FK_47D6465355CDE221 FOREIGN KEY (nomination_year_id) REFERENCES nomination_year (id)');
        $this->addSql('ALTER TABLE nomination_year_company ADD CONSTRAINT FK_47D64653979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE votes DROP FOREIGN KEY FK_518B7ACFA76ED395');
        $this->addSql('ALTER TABLE votes DROP FOREIGN KEY FK_518B7ACF55CDE221');
        $this->addSql('ALTER TABLE votes DROP FOREIGN KEY FK_518B7ACF979B1AD6');
        $this->addSql('ALTER TABLE nomination_year_company DROP FOREIGN KEY FK_47D64653979B1AD6');
        $this->addSql('ALTER TABLE nomination_year DROP FOREIGN KEY FK_6F1AD43912469DE2');
        $this->addSql('ALTER TABLE nomination_year_company DROP FOREIGN KEY FK_47D6465355CDE221');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE votes');
        $this->addSql('DROP TABLE nomination_year');
        $this->addSql('DROP TABLE nomination_year_company');
    }
}
