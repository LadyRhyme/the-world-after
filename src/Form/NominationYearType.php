<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Category;
use App\Entity\NominationYear;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NominationYearType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateNominationYear')
            ->add('company',EntityType::class,[
                'class' => Company::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true
            ])
            ->add('category',EntityType::class,[
                'class' => Category::class,
                'choice_label' => 'category'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NominationYear::class,
        ]);
    }
}
