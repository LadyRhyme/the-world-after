<?php

namespace App\DataFixtures;

use App\Entity\NominationYear;
use App\Repository\CategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NominationYearFixtures extends Fixture
{
    private $categories;

    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    public function load(ObjectManager $manager)
    {
        $nbcategories = $this->categories->count([]);

        for ($i= $nbcategories; $i > 0 ; $i--) { 

            $nominationyear = new NominationYear();
            $nominationyear->setCategory($this->categories->findOneBy(["id" => $i]));
            $interval = $nbcategories - $i;
            $datenominationYear = (new \DateTime("now"))->add(new \DateInterval("PT{$interval}H"));
            $nominationyear->setDateNominationYear($datenominationYear);
            $manager->persist($nominationyear);
        }

        $manager->flush();
    }
}