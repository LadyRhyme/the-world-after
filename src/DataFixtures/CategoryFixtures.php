<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Category;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Bezhanov\Faker\ProviderCollectionHelper;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CategoryFixtures extends Fixture
{   
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create("fr_FR");
    }

    private function uploadimagefromurl(Category $category,string $url)
    {
        $info = pathinfo($url);
        $contents = file_get_contents($url);
        $imagefilename = new Slugify();
        $filename = $imagefilename->slugify($this->faker->department) . "." . $info["extension"];
        $category->setDocument($filename);
        $dirnamedirectory = dirname(\dirname(__DIR__)) . "/public/images/";

        if(file_exists($dirnamedirectory)){
            $directory = $dirnamedirectory ."categorys/";
        }else{
            mkdir($dirnamedirectory);
            $directory = $dirnamedirectory . "categorys/";
        }

        if (file_exists($directory)) {
            $file = $directory . $filename;
        } else {
            mkdir($directory);
            $file = $directory . $filename;
        }

        file_put_contents($file, $contents);
        $imagefile = new UploadedFile($file, $filename);
        //dd($imagefile);
    }

     public function load(ObjectManager $manager)
    {
        ProviderCollectionHelper::addAllProvidersTo($this->faker);

        for ($i=1; $i < 11; $i++) { 

            $category = new Category();
            $category->setCategory($this->faker->department);
            $urlimg = "https://i.picsum.photos/id/{$i}/300/300.jpg";
            $this->uploadimagefromurl($category,$urlimg);
            $category->setImageFile(null);
            $manager->persist($category);
            
        }
        $manager->flush();
    }
}